# -*- coding: utf-8 -*-
from flask import Flask, render_template, jsonify
import dynamickey, json, datetime, os
from urllib2 import urlopen
import time


app = Flask(__name__)
app.config.update(dict(
    SECRET_KEY=dynamickey.SECRET_KEY,
    DEBUG=False
))
app.config.from_envvar('FLASK_RATES_SETTINGS', silent=True)



@app.route('/')
def index():
    return render_template('index.html')


@app.route('/<moeda>')
def convert(moeda):
    date_end = datetime.date.today()
    date_start = date_end - datetime.timedelta(days=6)

    url = 'http://jsonrates.com/historical/?from=%s&to=BRL&dateStart=%s&dateEnd=%s' %\
        (moeda, date_start.isoformat(), date_end.isoformat())

    response = urlopen(url)
    data = json.loads(response.read())
    result = {
        'moeda': data['from'],
        'values': {
            '%s/%s/%s' % (
            time.strptime(item, '%Y-%m-%d')[2], 
            time.strptime(item, '%Y-%m-%d')[1], 
            time.strptime(item, '%Y-%m-%d')[0]): data['rates'][item]['rate']
        for item in data['rates'].keys()}
    }

    return jsonify(result)



if __name__ == "__main__":
    port = int(os.environ.get('PORT', 5000))
    app.run(host='0.0.0.0', port=port)
