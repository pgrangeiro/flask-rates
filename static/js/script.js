var Chart = function (){
	this.options = {};
	this.moedas = {
		USD: 'Dólar',
		ARS: 'Peso Argentino',
		EUR: 'Euro'
	};

	this.init = function(data) {
		var labels = [];
		var series = [];

		for (var key in data.values) {
			labels.push(key);
			series.push(parseFloat(data.values[key]));
		}

		this.options = { 
	        chart: {
	            type: 'area'
	        },
	        title: {
	            text: this.moedas[data.moeda]
	        },
		    xAxis: {
		        categories: labels,
		    },
		    yAxis: {
            	allowDecimals: true,
		        title: {
		            text: 'Variação'
		        },
		    },
		    tooltip: {
		    	formatter: function() {
		    		return this.y.toFixed(4);
		    	}
		    },
		    plotOptions: {
		        line: {
		            dataLabels: {
		                enabled: true
		            },
		            enableMouseTracking: false
		        }
		    },
		    series: [{
		    	name: this.moedas[data.moeda],
            	threshold: null,
		    	showInLegend: false,
		    	data: series
		    }]
		};
	};
};

$().ready(function() {
	$("input").click(function() {
		var id = $(this).attr('id');

		$.ajax({
			url: '/' + id,
			dataType: 'json'
		}).done(function(data) {
			var chart = new Chart();
			chart.init(data);

			$("#chart").highcharts(chart.options);
			$("#chart").show();
		});
	});
});