# -*- coding: utf-8 -*-
import flask_rates
import unittest


class FlaskrTestCase(unittest.TestCase):

    def setUp(self):
        self.app = flask_rates.app.test_client()


    def test_index(self):
        rv = self.app.get('/')
        assert 'Selecione a moeda para exibir a cotação nos últimos 7 dias' in rv.data


    def test_exchange(self):
        rv = self.app.get('/USD')
        assert '"moeda": "USD"' in rv.data

        rv = self.app.get('/ARS')
        assert '"moeda": "ARS"' in rv.data

        rv = self.app.get('/EUR')
        assert '"moeda": "EUR"' in rv.data

if __name__ == '__main__':
    unittest.main()